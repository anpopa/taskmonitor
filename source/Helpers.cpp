/*-
 * SPDX-License-Identifier: MIT
 *-
 * @date      2021-2022
 * @author    Alin Popa <alin.popa@fxdata.ro>
 * @copyright MIT
 * @brief     Helper methods
 * @details   Verious helper methods
 *-
 */

#include "Helpers.h"
#include "../bswinfra/source/Logger.h"
#include <cstddef>
#include <taskmonitor/Helpers.h>
#include <taskmonitor/taskmonitor.h>
#ifdef WITH_LXC
#include <lxc/lxccontainer.h>
#endif
#include <fstream>

namespace tkm
{

static uint64_t pidOneCtxId = 0;

#ifdef WITH_LXC
static auto getContainerNameForContext(const std::string &contPath, uint64_t ctxId) -> std::string
{
  struct lxc_container **activeContainers = NULL;
  std::string contName{"unknown"};
  char **names = NULL;
  bool found = false;

  auto count = list_active_containers(contPath.c_str(), &names, &activeContainers);
  for (int i = 0; i < count && !found; i++) {
    struct lxc_container *container = activeContainers[i];
    const char *name = names[i];

    if (name == NULL || container == NULL) {
      continue;
    }

    if (container->is_running(container)) {
      auto pid = container->init_pid(container);
      if (getContextId(pid) == ctxId) {
        contName = std::string(name);
        found = true;
      }
    }
  }

  for (int i = 0; i < count; i++) {
    free(names[i]);
    lxc_container_put(activeContainers[i]);
  }
  free(names);
  free(activeContainers);

  return contName;
}
#endif

#ifdef WITH_CGROUPS_CONTEXT
static auto getContainerNameForProcess(const std::string &groupPath, uint64_t pId) -> std::string
{
  std::ifstream cgroupStream{"/proc/" + std::to_string(pId) + "/cgroup"};
  std::string contName{"unknown"};

  if (!cgroupStream.is_open()) {
    throw std::runtime_error("Fail to open /proc/" + std::to_string(pId) + "/cgroup file");
  }

  std::string line; // only need the first line
  if (!std::getline(cgroupStream, line)) {
    throw std::runtime_error("Fail to read /proc/" + std::to_string(pId) + "/cgroup file");
  }

  std::vector<std::string> tokens;
  std::stringstream ss(line);
  std::string buf;
  while (std::getline(ss, buf, ':')) {
    tokens.push_back(buf);
  }

  if (tokens.size() != 3) {
    throw std::runtime_error("Parse fail for /proc/" + std::to_string(pId) + "/cgroup file");
  }

  const auto cgPath = tokens[2];
  const auto pos = cgPath.rfind(groupPath, 0);
  if (pos != std::string::npos) {
    contName = cgPath.substr(groupPath.size());
    if (contName.find('/') != std::string::npos) {
      std::stringstream sss(contName);
      tokens.clear();
      while (std::getline(sss, buf, '/')) {
        tokens.push_back(buf);
      }
      contName = tokens[0]; // should have at least the first token
    }
  }

  return contName;
}
#endif

auto getLXCContextName(const std::string &contPath, uint64_t ctxId) -> std::string
{
  if (pidOneCtxId == 0) {
    pidOneCtxId = tkm::getContextId(1);
  }
  if (ctxId == pidOneCtxId) {
    return std::string{"root"};
  }
#ifdef WITH_LXC
  return getContainerNameForContext(contPath, ctxId);
#else
  static_cast<void>(contPath); // UNUSED
  return std::string("unknown");
#endif
}

auto getGRPContextName(const std::string &groupPath, uint64_t ctxId, uint64_t pId) -> std::string
{
  if (pidOneCtxId == 0) {
    pidOneCtxId = tkm::getContextId(1);
  }
  if (ctxId == pidOneCtxId) {
    return std::string{"root"};
  }
#ifdef WITH_CGROUPS_CONTEXT
  auto name = std::string("unknown");

  try {
    name = getContainerNameForProcess(groupPath, pId);
  } catch (std::exception &e) {
    logError() << "Fail resolve context name for PID " << pId << ". Exception: " << e.what();
  }

  return name;
#else
  static_cast<void>(groupPath); // UNUSED
  static_cast<void>(pId);       // UNUSED
  return std::string("unknown");
#endif
}

} // namespace tkm
