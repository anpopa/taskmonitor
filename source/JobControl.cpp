/*-
 * SPDX-License-Identifier: MIT
 *-
 * @date      2021-2024
 * @author    Alin Popa <alin.popa@triboo-tech.ro>
 * @copyright MIT
 * @brief     JobControl Class
 * @details   Manage external processes STOP/CONTINUE signals
 *-
 */

#include <csignal>
#include <string>

#include "Application.h"
#include "JobControl.h"
#include "Logger.h"
#include "ProcEntry.h"

#include <signal.h>

namespace tkm::monitor
{

static bool doTrigger(const std::shared_ptr<JobControl> mgr, const JobControl::Request &rq);

JobControl::JobControl(const std::shared_ptr<Options> options)
: m_options(options)
{
  m_queue = std::make_shared<AsyncQueue<Request>>(
      "JobControlEventQueue", [this](const Request &request) { return requestHandler(request); });
}

auto JobControl::pushRequest(Request &request) -> int
{
  return m_queue->push(request);
}

void JobControl::setEventSource(bool enabled)
{
  if (enabled) {
    App()->addEventSource(m_queue);
  } else {
    App()->remEventSource(m_queue);
  }
}

auto JobControl::requestHandler(const JobControl::Request &request) -> bool
{
  switch (request.action) {
  case JobControl::Action::Connect:
    return resetConnection();
  case JobControl::Action::Cleanup:
    return triggerCleanup();
  case JobControl::Action::RegisterSignal:
    return registerForGroupSignal();
  case JobControl::Action::Trigger:
    return doTrigger(getShared(), request);
  default:
    break;
  }

  logError() << "Unknown action request";
  return false;
}

bool JobControl::resetConnection()
{
  // Reset any existing connection
  m_sdbusConnection.reset();
  m_sdbusObject.reset();

  const std::string serviceName{"org.tkm.taskmonitor"};
  m_sdbusConnection = sdbus::createSystemBusConnection(serviceName);

  sdbus::ObjectPath objectPath{"/org/tkm/taskmonitor/jobcontrol"};
  m_sdbusObject = sdbus::createObject(*m_sdbusConnection, std::move(objectPath));

  auto triggerGroup = [this](const std::string &groupName) {
    if (getInProgress()) {
      logWarn() << "Trigger jobcontrol for group: " << groupName << " while in progress";
      return false;
    }
    setInProgress(true);

    JobControl::Request request = {.action = JobControl::Action::Trigger, .arg = groupName};
    pushRequest(request);

    return true;
  };

  const std::string interfaceName{"org.tkm.taskmonitor.jobcontrol"};
  m_sdbusObject->registerMethod("trigger")
      .onInterface(interfaceName)
      .implementedAs(std::move(triggerGroup));
  m_sdbusObject->registerSignal("triggered")
      .onInterface(interfaceName)
      .withParameters<std::string>();
  m_sdbusObject->finishRegistration();

  m_sdbusConnection->enterEventLoopAsync();
  m_registered = true;

  logInfo() << "JobControl server registered";

  const auto signalGroupName = m_options->getFor(Options::Key::JCTriggerGroupOnSignal);
  if (!signalGroupName.empty()) {
    if (signalGroupName != "none") {
      logDebug() << "JobControl group on signal configured. Request registration";
      JobControl::Request request = {.action = JobControl::Action::RegisterSignal, .arg = ""};
      pushRequest(request);
    }
  }

  return true;
}

bool JobControl::triggerGroup(const std::string &groupName)
{
  if (!m_options->hasConfigFile()) {
    return true;
  }
  const std::vector<bswi::kf::Property> props =
      m_options->getConfigFile()->getProperties("jobcontrol", -1);
  const std::string groupToken = groupName + '.';

  std::string groupEntries{};
  std::string groupTimeout{};

  for (const auto &prop : props) {
    if (prop.key.rfind(groupToken, 0) != std::string::npos) {
      const std::string subKey = prop.key.substr(prop.key.find(".") + 1);
      if (subKey == "Entries") {
        groupEntries = prop.value;
      } else if (subKey == "Timeout") {
        groupTimeout = prop.value;
      }
    }
  }

  if (groupEntries.empty()) {
    logWarn() << "Jobcontrol group entries empty for group: " << groupName;
    return false;
  }

  std::vector<std::string> groupEntriesVec{};
  std::stringstream buffer(groupEntries);
  std::string segment;
  while (std::getline(buffer, segment, ' ')) {
    groupEntriesVec.push_back(segment);
  }

  unsigned int groupTimeoutUs =
      std::stoul(m_options->getFor(Options::Key::JCDefaultSuspendTimeout));
  try {
    auto interval = std::stoul(groupTimeout);
    if (interval >= 100000) {
      groupTimeoutUs = interval;
    }
  } catch (...) {
    logError() << "Failed to convert DefaultSuspendTimeout. Using default";
  }

  logDebug() << "Jobcontrol triggered for group: " << groupName << " timeout:" << groupTimeoutUs
             << " entries: " << groupEntries;

  // Send SIGSTOP signal to all control group processes
  App()->getProcRegistry()->getProcList().foreach (
      [this, &groupEntriesVec](const std::shared_ptr<ProcEntry> &entry) {
        for (const auto &groupEntryName : groupEntriesVec) {
          if (entry->getName() == groupEntryName) {
            // We signal based on process name only since entry->getState() is expensive
            logDebug() << "JobControl stop process: " << entry->getName()
                       << " with PID:" << entry->getPid();
            kill(entry->getPid(), SIGSTOP);
          }
        }
      });

  // Create and start the resume timer
  const auto resumeTimer = std::make_shared<Timer>("JobControl", [this, groupEntriesVec]() {
    App()->getProcRegistry()->getProcList().foreach (
        [this, &groupEntriesVec](const std::shared_ptr<ProcEntry> &entry) {
          for (const auto &groupEntryName : groupEntriesVec) {
            if (entry->getName() == groupEntryName) {
              // We signal based on process name only since entry->getState() is expensive
              logDebug() << "JobControl continue process: " << entry->getName()
                         << " with PID:" << entry->getPid();
              kill(entry->getPid(), SIGCONT);
            }
          }
        });
    setInProgress(false);
    return false;
  });

  resumeTimer->start(groupTimeoutUs, false);
  App()->addEventSource(resumeTimer);

  return true;
}

bool JobControl::triggerCleanup()
{
  if (!m_options->hasConfigFile()) {
    return true;
  }
  logDebug() << "JobControl cleanup action";

  const std::vector<bswi::kf::Property> props =
      m_options->getConfigFile()->getProperties("jobcontrol", -1);
  std::vector<std::string> entriesVec{};

  for (const auto &prop : props) {
    std::vector<std::string> groupTokens{};
    std::stringstream buf1(prop.key);
    std::string segment;

    while (std::getline(buf1, segment, '.')) {
      groupTokens.push_back(segment);
    }

    if (groupTokens.size() != 2) {
      continue;
    }

    // OK, we are in a group property entry
    std::stringstream buf2(prop.value);
    while (std::getline(buf2, segment, ' ')) {
      if (std::find(entriesVec.begin(), entriesVec.end(), segment) == entriesVec.end()) {
        entriesVec.push_back(segment);
      }
    }
  }

  if (!entriesVec.empty()) {
    App()->getProcRegistry()->getProcList().foreach (
        [this, &entriesVec](const std::shared_ptr<ProcEntry> &entry) {
          for (const auto &entryName : entriesVec) {
            if (entry->getName() == entryName) {
              if (entry->getState() == ProcEntry::State::Stopped) {
                logDebug() << "JobControl cleanup process: " << entry->getName()
                           << " with PID:" << entry->getPid();
                kill(entry->getPid(), SIGCONT);
              }
            }
          }
        });
  }

  return true;
}

bool JobControl::registerForGroupSignal()
{
  const auto signalDestination = m_options->getFor(Options::Key::JCSignalDestination);
  const auto signalPath = m_options->getFor(Options::Key::JCSignalPath);
  const auto signalInterface = m_options->getFor(Options::Key::JCSignalInterface);
  const auto signalName = m_options->getFor(Options::Key::JCSignalEventName);
  const auto groupName = m_options->getFor(Options::Key::JCTriggerGroupOnSignal);

  if (groupName.empty() || signalDestination.empty() || signalPath.empty() ||
      signalInterface.empty() || signalName.empty()) {
    logWarn() << "Empty JobControl signal configuration";
    return true;
  }

  if (groupName == "none" || signalDestination == "none" || signalPath == "none" ||
      signalInterface == "none" || signalName == "none") {
    logWarn() << "Invalid JobControl signal configuration";
    return true;
  }

  logDebug() << "JobControl register on dest: " << signalDestination << " path: " << signalPath
             << " interface: " << signalInterface << " name: " << signalName;
  m_sdbusSignalProxy = sdbus::createProxy(signalDestination, signalPath);
  m_sdbusSignalProxy->uponSignal(signalName).onInterface(signalInterface).call([this, groupName]() {
    if (getInProgress()) {
      logWarn() << "Trigger jobcontrol for group: " << groupName << " while in progress";
    } else {
      setInProgress(true);
      JobControl::Request request = {.action = JobControl::Action::Trigger, .arg = groupName};
      pushRequest(request);
    }
  });
  m_sdbusSignalProxy->finishRegistration();

  return true;
}

static bool doTrigger(const std::shared_ptr<JobControl> mgr, const JobControl::Request &rq)
{
  if (!mgr->getRegistered()) {
    logError() << "JobControl DBus server not registered";
    return true;
  }

  if (mgr->triggerGroup(rq.arg)) {
    mgr->getSDBusObject()
        ->emitSignal("triggered")
        .onInterface("org.tkm.taskmonitor.jobcontrol")
        .withArguments(rq.arg);
  } else {
    mgr->setInProgress(false);
  }

  return true;
}

} // namespace tkm::monitor
