/*-
 * SPDX-License-Identifier: MIT
 *-
 * @date      2021-2022
 * @author    Alin Popa <alin.popa@fxdata.ro>
 * @copyright MIT
 * @brief     Helper methods
 * @details   Verious helper methods
 *-
 */

#pragma once

#include <cstdint>
#include <string>

namespace tkm
{

auto getLXCContextName(const std::string &contPath, uint64_t ctxId) -> std::string;
auto getGRPContextName(const std::string &groupPath, uint64_t ctxId, uint64_t pId) -> std::string;

} // namespace tkm
